## Assessment of the final project

You will show your work on the final project in the form of an oral presentation.

While this is different from the earlier reports you wrote, we are still looking
for the content for the same elements as in the report. In particular, these
criteria also apply to the presentation:

- Explains the theoretical background *and the motivation for the research question*
- Documents correctness checks (for the final projects, there may not be a big
  distinction between validity check and final result, if you are reproducing
  the result of a paper)
- Data is reported with uncertainty, and fitted to the expected behavior if
  applicable
- A variety of phenomena are investigated (varies per project), and compared
  with known results
- Performance is reported and discussed briefly

In addition to that, also the presentation and your presentation skills will
enter the grade.

Note that we do not expect you to disseminate the group aspect of the work, but
rather that you do the presentation as a group.

Contrary to the report, we will not give separate grades for all of these
aspects, but a single grade taking into account all these aspects.

We will not grade code or repository (review issues) for the final project

## Brief description of the predefined final projects

- `ComputationalAstrophysics`:
  Implement the Barnes-Hut algorithm which is an efficient approximation technique to simulate the gravitational interaction
  of many bodies. Use it to simulate for example a galaxy!
- `LatticeBoltzmann`:
  Simulate fluid dynamics (including e.g. turbulence) using a lattice-based description
- `LatticeBoltzmannWithObjects`:
  Simulate movable objects swimming in a fluid using a lattice-based description
- `MarchOfThePenguins`:
  Simulate a soft-matter system inspired by the dynamics of groups of pinguins in Antarctica
- `ParallelMD`:
  Implement a parallelized (MPI-version) of the molecular dynamics program of the first project
- `Piano`:
  Simulate a piano by simulating the vibrations of real string hit by a hammer. Connect it to your laptop speakers
  and hear your virtual piano playing.
- `QMWavePropagation`:
  Simulate the propagation of quantummechanical wave packets and see e.g. interference happening
- `QuantumTrajectories`:
  Simulate open quantum systems
- `SelfOrganizedCriticality`:
  Simualte the patterns created by complex processes in nature
- `SmoothParticleHydrodynamics`:
  Simulate the behavior of fluids using an approximate method and simulate water splashing around

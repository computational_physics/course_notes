## Code (weight 0.3):
* Critical (always 5 points):
    - The code works
* Important (up to 1 point each):
    - Simulation is separate from execution and data processing (performed in different modules or module + notebook)
    - The only global variables are physical or simulation constants
    - The code is organized according to the simulation logic and uses functions (or classes)
    - When possible and reasonable, efficient functions from scipy stack libraries are used
* Minor (up to 0.5 points each):
    - Docstrings correctly explain how to use each function
    - Consistent formatting and naming conventions (e.g. following PEP8)
    - Nontrivial (and only nontrivial) parts of code are commented
    - Variable and function names are understandable and follow a logical scheme
* Minor penalty (0.5 points):
    - Mixing different spoken languages (we recommend English-only)

## Repository (weight 0.2):
- (6 points) Weekly issues report the progress, correctness checks, and identify bugs or follow-up steps
- (2 points) Every project member makes regular commits
- (1 point) Commit messages are descriptive
- (1 point) No unnecessary/temporary/duplicate files

## Report (weight 0.5):
*  Major (0–2 points):
    - Explains the theoretical background
    - Documents correctness checks
    - Data is reported with uncertainty, and fitted to the expected behavior if applicable
    - A variety of phenomena are investigated (varies per project), and compared with known results
*  Minor (0–1 point):
    - Performance is reported and discussed
    - Individual contributions and work organization are documented
    - Figures and data presentation fulfill publication standards
    - Report is as a short paper with abstract, introduction, methods, results, conclusions and references.
